# demo

Geolocation : 
1- https://developer.mozilla.org/en-US/docs/Web/API/Geolocation_API

Unplash :
export default axios.create({
    baseURL:'https://api.unplash.com',
    headers : {
        Authorization: 'Client-ID xxxxxxx'
    }
})

Array : 1) state.pop (Remove item)        <=> state.filter(element => element !== 'hi')
        2) state.push (Add item)          <=> [...state,'hi']
        3) state[0] = 'hi' (Replace item) <=> state.map(el => el === 'hi'?'bye':el)

Object : 1) state.name = 'Sam' (Update item)  <=> { ...state,name:'Sam'}
         2) state.age = 30 (Add item)         <=> { ...state,age:30}
         3) delete state.name (Remove item)   <=> {...state,age:undefined } 
                                                  Or _.omit(state,'age')