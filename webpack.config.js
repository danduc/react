const HtmlWebPackPlugin = require ("html-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
    optimization: {
       minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader:"babel-loader"
                }
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader:"html-loader"
                    }
                ]
            },
            { 
                test: /\.css$/i, 
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            //moduleFilename: ({ name }) => `${name.replace('/js/', '/css/')}.css`,
            filename: 'main.css',
             chunkFilename: 'style.css',
        }),
        new HtmlWebPackPlugin({
            template: "./public/index.html",
            filename:"./index.html"
        }) 
    ]
}