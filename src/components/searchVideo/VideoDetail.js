import React from 'react';
import htmlEntitiy from 'he';

const renderVideo = (video) => {
    const videoSrc = `https://www.youtube.com/embed/${video.id.videoId}`
    return (<div className='ui segment'>
               <div className='ui embed'>
                   <iframe title='video player' src={ videoSrc } />
               </div>
               <h4 className='ui header'> { htmlEntitiy.decode(video.snippet.title)} </h4>
               <p>{ htmlEntitiy.decode(video.snippet.description) }</p>
            </div>);
}

const VideoDetail = ({ video, defaultVideo }) => {
    
    if(video != null){
        return renderVideo(video);  
    }else{
       if(defaultVideo != "")
       {
           return renderVideo(defaultVideo);
       }else{
           return <div></div>;
       }
    }
}

export default VideoDetail;