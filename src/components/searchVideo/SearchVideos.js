import React from 'react';
import SearchVideoBar from './SearchVideoBar';
import axios from 'axios';
import VideoList from './VideoList';
import VideoDetail from './VideoDetail';

export default class SearchVideos extends React.Component
{
    
    state = { videos : [], selectedVideo : null, defaultVideo : "" }
    
    onTermSubmit = async term => {
        
        const response = await axios.get('https://www.googleapis.com/youtube/v3/search',{
                                            params : {
                                                key : 'AIzaSyACj5wFvuQDdzqQD1xKo6mO-0nV6Rp_wpQ',
                                                part : 'snippet',
                                                maxResults : 5,
                                                q : term
                                            }
                                         });
        
        this.setState({ videos : response.data.items, defaultVideo : response.data.items[0] });
      
    }
    
    onVideoSelect = video => {
        this.setState({ selectedVideo : video });
    }
    
    render(){
        return <div>
                   <div className='ui grid'>
                        <div className='ui row'>
                            <div className='six wide column'>
                               <SearchVideoBar onFormSubmit={ this.onTermSubmit } />
                            </div>
                        </div>
                       <div className='ui row'>
                           <div className='eleven wide column'>
                               <VideoDetail video={ this.state.selectedVideo } defaultVideo={ this.state.defaultVideo } />
                           </div>
                           <div className='five wide column'>
                               <VideoList onVideoSelect={ this.onVideoSelect } videos={ this.state.videos } />
                           </div>
                        </div>
                    </div>
                </div>;
    }
    
};