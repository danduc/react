import React from 'react';
import ReactDom from 'react-dom';

export default class SearchVideoBar extends React.Component
{
    state = { term : ''}

    onInputChange = event => {
        this.setState({ term : event.target.value });
    }
    
    onFormSubmit = event => {
        event.preventDefault();
        this.props.onFormSubmit(this.state.term);  
    }
    
    render(){
        return (<div className='ui segment'>
                    <form className='ui form' onSubmit={ this.onFormSubmit }>
                        <div className='field'>
                           <label>Video Search</label>
                           <div className='ui icon input'>
                                <input type='text' onChange={ this.onInputChange } placeholder='Search...' />
                                <i className='search icon'></i>
                           </div>
                        </div>
                    </form>
                </div>);
    }
}

