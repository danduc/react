import React from 'react';
import ReactDom from 'react-dom';
import axios from 'axios';
import SearchPhotoBar from './SearchPhotoBar';
import 'babel-polyfill';
import ImageList from './ImageList';


export default class Search extends React.Component
{
    
    state = { images : [] };

    onSearchSubmit = async term => 
    {
        const response = await axios.get('https://api.unsplash.com/search/photos',{
            params : { query : term },
            headers : {
                Authorization : 'Client-ID a7cd0e3356a5dcf1574ae7d63f94e87d0f131650365f897c4d7a4f8fae2ae8ae'
            }
        });
        this.setState({ images : response.data.results } );  
    }
    
    render()
    {
        return (
          <div>
             <div className='ui grid'>
                <div className='ui row'>
                    <div className='six wide column'>
                        <SearchPhotoBar onSubmit={ this.onSearchSubmit } />
                    </div>
                </div>
                <div className='ui row'>
                    <div className='sixteen wide column'>
                        <ImageList images={ this.state.images } />
                    </div>
                </div>
             </div>
          </div> 
        )
    }
        
}