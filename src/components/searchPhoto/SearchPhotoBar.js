import React from 'react';
import ReactDOM from 'react-dom';

export default class SearchBar extends React.Component
{
    state = { term : '' }

    onFormSubmit = event => {
        event.preventDefault();
        this.props.onSubmit(this.state.term);
    }
    
    render()
    {
        return  <div className='ui segment'>
                    <form className='ui form' onSubmit={ this.onFormSubmit }>
                        <div className='field'>
                           <label>Photo Search</label>
                           <div className='ui icon input'>
                                <input type='text' onChange={ event => this.setState({ term : event.target.value }) } placeholder='Search...' />
                                <i className='search icon'></i>
                           </div>
                        </div>
                    </form>
                </div>;
    }
}



                                