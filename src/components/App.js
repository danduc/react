import React from 'react';
import ReactDom from 'react-dom';
import { HashRouter as Router,
  Switch,
  Route,
  Link } from 'react-router-dom';
import SearchVideos  from './searchVideo/SearchVideos';
import Blog  from './comment/Blog';
import Seasons from './season/Seasons';
import SearchPhotos from './searchPhoto/SearchPhotos';
import { Dropdown, Menu, NavLink } from 'semantic-ui-react';

export default class App extends React.Component {
    
    state = { activeItem: 'Search Videos' }

    handleItemClick = (e, { name }) => this.setState({ activeItem: name });

    render()
    {
        return(
                <Router>
                    <div>
                        <Menu size='large'>
                             <Menu.Item
                                 name='Search Videos'
                                 active={this.state.activeItem === 'Search Videos'}
                                 onClick={this.handleItemClick}
                                 to="/"
                                 as={Link} />
                              <Menu.Item
                                 name='Blog'
                                 active={this.state.activeItem === 'Blog'}
                                 onClick={this.handleItemClick}
                                 to="/blog"
                                 as={Link} />
                               <Menu.Item
                                 name='Seasons'
                                 active={this.state.activeItem === 'Seasons'}
                                 onClick={this.handleItemClick}
                                 to="/seasons"
                                 as={Link} />
                               <Menu.Item
                                 name='Search Photos'
                                 active={this.state.activeItem === 'Search Photos'}
                                 onClick={this.handleItemClick}
                                 to="/SearchPhotos"
                                 as={Link} />
                         </Menu>
                         <Switch>
                             <Route exact path="/">
                               <SearchVideos />
                             </Route>
                             <Route path="/blog">
                               <Blog />
                             </Route>
                             <Route path="/seasons">
                               <Seasons />
                             </Route>
                             <Route path="/searchPhotos">
                               <SearchPhotos />
                             </Route>
                           </Switch>
                        </div>  
                     </Router>
              );
       }
}
             