import React from 'react';
import ReactDOM from 'react-dom';
import SeasonDisplay from './SeasonDisplay';
import Spinner from './Spinner';

export default class Seasons extends React.Component
{
    state = { lat : null, errorMessage : ''};

    componentDidMount(){
         window.navigator.geolocation.getCurrentPosition(
            position => this.setState({ lat : position.coords.latitude }),
            error => this.setState({ errorMessage : error })
        );
        
    }

    renderContent = () => 
    {
         if(this.state.errorMessage && !this.state.lat)
        {
           return (<div className='border red'><div>Error: { this.state.errorMessage }</div></div>);
        }
         
        if(!this.state.errorMessage && this.state.lat)
        {
                return <div className='border red'><SeasonDisplay lat={ this.state.lat } /></div>
        } 
        
        return <div className='border red'><Spinner message='Please accept location request' /></div>;
    }

    render(){
            return <div>{this.renderContent()}</div>;
    }
}
