import React from 'react';
import faker from 'faker';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';
const Blog = () => {
    return (
           <div className = "ui comments">
                <ApprovalCard>
                <CommentDetail author="Sam" 
                               timeAgo="Today at 4:00" 
                               content="Nice blog post"
                               avatar={faker.image.avatar()}/>
                </ApprovalCard>
                <ApprovalCard>
                <CommentDetail author="Alex" 
                               timeAgo="Today at 12:00" 
                               content="I like the subject" 
                               avatar={faker.image.avatar()} />
                </ApprovalCard>
                <ApprovalCard>
                <CommentDetail author="Jan" 
                               timeAgo="Today at 06:00" 
                               content="I like the writing" 
                               avatar={faker.image.avatar()} />
                </ApprovalCard>
            </div>
    );
};


export default Blog;

